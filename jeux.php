<?php

class Player {
    
    private string $name;
    private int $hp;
    private int $level_attack;
    private int $level_defense; 

        public function __construct(string $name, int $hp, int $level_attack, int $level_defense)
        {

            $this-> name = $name;
            $this-> hp = $hp;
            $this-> level_attack = $level_attack;
            $this-> level_defense = $level_defense;

        }

        public function attack( Player $target) 
        {
            $push = rand(0,100);

            $totaldmg = $push + $this->level_attack;
            $dmglanded = $totaldmg - $target->getLevelDefense();
            $target->setHp($dmglanded - $target->getHp());
            
            $this->punchBack($target , $dmglanded);
        }

        public function punchBack(Player $target, int $domg)
        {
           $this -> setHp($target->getLevelDefense() - $domg);  
        }

        public function endGame(Player $target)
        {
            if($target->getHp()<= 0)
            {
                return true;
            }
            return false;
        }
 
        public function getName()
        {
            return $this->name;
        }

        public function setName(String $name)
        {
            $this->name = $name;
        }

        public function getHp()
        {
            return $this->hp;
        }

        public function setHp(int $hp)
        {
            $this->hp = $hp;
        }

        public function getLevelAttck()
        {
            return $this->level_attack;
        }

        public function setLevelAttack(int $level_attack)
        {
            $this->level_attack = $level_attack;
        }

        public function getLevelDefense()
        {
            return $this->level_defense;
        }

        public function setLevelDefense(int $level_defense)
        { 
            $this->level_defense = $level_defense;
        }
}
echo("hello");

